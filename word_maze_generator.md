# Word Maze Generator

Write a python code to generate word maze based on the input words.

Input:

- Mercury 
- Uranus
- Jupiter
- Saturn
- Neptune
- Earth
- Mars
- Venus

Output will be a 2D array with the maze elements

Sample input and output:

![image](./assets/planets.svg)


